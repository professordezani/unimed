import { TutorialPage } from './../tutorial/tutorial';
import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

@Component({
  selector: 'page-contact',
  templateUrl: 'contact.html'
})
export class ContactPage {

  constructor(public navCtrl: NavController) {

  }


  public openTutorial() : void {
    this.navCtrl.push(TutorialPage);
  }

}
