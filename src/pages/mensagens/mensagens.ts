import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AngularFireDatabase } from 'angularfire2/database';
import { Observable } from 'rxjs/Observable';

@IonicPage()
@Component({
  selector: 'page-mensagens',
  templateUrl: 'mensagens.html',
})
export class MensagensPage {

  public mensagens: Observable<Mensagem[]>;
  public mensagem: Mensagem;

  constructor(public navCtrl: NavController, public navParams: NavParams, public db: AngularFireDatabase) {
    this.mensagens = db.list('mensagens').valueChanges();
    this.mensagem = new Mensagem();
  }

  ionViewDidLoad() {
    
  }

  public send(mensagem : Mensagem) : void {
    mensagem.data = new Date().getTime();

    // TODO: Recuperar os dados do usuário conectado.
    mensagem.uid = "2";
    mensagem.displayName = "Beltrano";
    //mensagem.photoURL = "http://...";

    this.db.list('mensagens').push(mensagem).then(() => {
      mensagem.texto = "";
    })
  }
}


export class Mensagem {
  public texto: String = "";
  public data: number;
  public uid: String;
  public displayName: String;
  public photoURL: String;
}